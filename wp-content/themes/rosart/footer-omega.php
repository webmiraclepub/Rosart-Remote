		<!-- Тут подключат скрипты -->
		<?php wp_footer(); ?>
		<script type="text/javascript">
document.addEventListener( 'DOMContentLoaded', function(){
	var form = document.querySelectorAll('form');

	for( var i = 0; i < form.length; i++ ){
		form[i].addEventListener( 'submit', function(e){
			e.preventDefault();
			var body = {};
			var input = e.target.querySelectorAll('input');
			for (var i = 0; i < input.length; i++) {
				var key = input[i].getAttribute('name');
				var val = input[i].value;
				body[key] = val;
			}

			var url = '';
			for( let variable in body ) {
				if (body.hasOwnProperty(variable)) {
						url += variable + '=' + encodeURIComponent( body[variable] ) + '&';
				}
			}
			url += 'nonce=' + encodeURIComponent( ajax.nonce );

			var xhr = new XMLHttpRequest();

			xhr.open("POST", ajax.url, true);
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.onreadystatechange = function(){
				// console.log( arguments );
				if (xhr.readyState == 4 && xhr.status == 200) location.href = '<?= get_field( 'miracle-global-form-redirect', 'option' ) ?>';
			};

			xhr.send(url);
		});
	}
});
		</script>
	</body>
</html>
