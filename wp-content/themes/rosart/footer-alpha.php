<?php
    $theme_dir_uri = get_template_directory_uri( );
    $footer_style = ( !is_404() && !is_page_template( 'pagetemplates/main.php' ) );
    $bgi = get_field( 'footer-global-bg', 'option' );
    $bgi_lazy = ( $footer_style ) ? $bgi['sizes']['lazy'] : '';
    $bgi_full = ( $footer_style ) ? $bgi['url'] : '';

    $phone = get_field( 'miracle-global-phone-number', 'option' );
    $address = get_field( 'miracle-global-address', 'option' );
    $shedule = get_field( 'miracle-global-shedule', 'option' );
    $brif = get_field( 'miracle-global-company-brif', 'option' );
    $email = get_field( 'miracle-global-company-email', 'option' );
    $skype = get_field( 'miracle-global-company-skype', 'option' );
    $presentation = get_field( 'miracle-global-company-presentation', 'option' );
    $logo_src = get_field( 'miracle-global-logo-image', 'option' )['url'];
    $logo_alt = get_field( 'miracle-global-logo-image', 'option' )['alt'];
    $logo_title = get_field( 'miracle-global-logo-image', 'option' )['title'];
?>
<div style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center; position: relative;">
    <footer class="footer" data-src="<?= $bgi_full ?>">
        <div class="footer__medium">
            <ul class="footer__medium-menu">
                <?= miracle_get_menuitems( 'miracle_mobile_footer_menu', false, 'footer' ) ?>
            </ul>
            <div class="footer__phone">
                <p class="footer__phone-number"><?= $phone ?></p>
                <button class="footer__modal-button miracle-button" data-modal="miracle-modal-send-list" type="button">Оставить заявку</button>
            </div>
        </div>
        <div class="footer__top">
            <div class="footer__top-content">
                <div class="footer__company-info">
                    <i class="footer__company-info-icon miracle-font-adress"></i>
                    <h4 class="footer__company-info-title">Наш адрес</h4>
                    <p class="footer__company-info-content"><?= $address ?></p>
                </div>
                <div class="footer__company-info">
                    <i class="footer__company-info-icon miracle-font-clock"></i>
                    <h4 class="footer__company-info-title">График работы</h4>
                    <p class="footer__company-info-content"><?= $shedule ?></p>
                </div>
                <div class="footer__company-info footer__company-info_push-down">
                    <i class="footer__company-info-icon miracle-font-brif"></i>
                    <a class="footer__company-info-title" href="<?= $brif ?>" download>Скачать бриф</a>
                    <p class="footer__company-info-content">На разработку сайта</p>
                </div>
                <div class="footer__company-info">
                    <i class="footer__company-info-icon miracle-font-mail"></i>
                    <h4 class="footer__company-info-title">E-mail</h4>
                    <p class="footer__company-info-content"><?= $email ?></p>
                </div>
                <div class="footer__company-info">
                    <i class="footer__company-info-icon miracle-font-skype"></i>
                    <h4 class="footer__company-info-title">Skype</h4>
                    <p class="footer__company-info-content"><?= $skype ?></p>
                </div>
                <div class="footer__company-info footer__company-info_push-down">
                    <i class="footer__company-info-icon miracle-font-presentation"></i>
                    <a class="footer__company-info-title" href="<?= $presentation ?>" download>Скачать презентацию</a>
                    <p class="footer__company-info-content">О нашей компании</p>
                </div>
                <div class="footer__phone footer__phone_show-large">
                    <p class="footer__phone-number"><?= $phone ?></p>
                    <button class="footer__modal-button miracle-button" data-modal="miracle-modal-send-list" type="button">Оставить заявку</button>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__bottom-content">
                <div class="footer__logo logotype">
                    <img class="logotype__image" data-src="<?= $logo_src ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>"/>
                    <p class="logotype__text">2017. Все права защищены</p>
                </div>
                <div class="footer__social">
    <?php
        if ( function_exists('dynamic_sidebar') ):
            dynamic_sidebar('social-icon');
        endif;
    ?>
                </div>
            </div>
        </div>
    </footer>
</div>
