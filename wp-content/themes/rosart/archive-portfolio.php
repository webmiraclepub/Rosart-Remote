<?php

get_header();

$theme_dir = get_template_directory( ) . '/views_support/';
$theme_dir_uri = get_template_directory_uri( );

$active_all = 'archive-filter__filters_active';
$tax_link = get_post_type_archive_link( 'portfolio' );
$bgi = get_field( 'miracle-global-header-bg', 'option' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
$breadcrumbs = miracle_get_breadcrumbs( array( 'Главная' => get_home_url() ) );
$title ='Портфолио';
$filters = miracle_get_portfolio_filter();
include( $theme_dir . 'portfolio/portfolio-header.php' );

$portfolio_posts = miracle_get_portfolio_post_cards();
$pagenation = miracle_get_pagenation();
include( $theme_dir . 'portfolio/archive-body.php' );

$title = get_field( 'miracle-global-send-app-title', 'option' );
$subtitle = get_field( 'miracle-global-send-app-subtitle', 'option' );
$content = get_field( 'miracle-global-send-app-content', 'option' );
$bgi = get_field( 'miracle-global-send-app-bg', 'option' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
$image = get_field( 'miracle-global-send-app-image', 'option' );
$image_full = $image['url'];
$image_lazy = $image['sizes']['lazy'];
$image_alt = $image['alt'];
$image_title = $image['title'];
include( $theme_dir . 'global/app-form.php' );

$title = get_field( 'miracle-global-sertificate-title', 'option' );
$bgi = get_field( 'miracle-global-sertificate-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-sertificate-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/sertificate.php' );

$title = get_field( 'miracle-global-trust-title', 'option' );
$bgi = get_field( 'miracle-global-trust-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-trust-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/parthner.php' );

$form1 = miracle_get_global_form( 'send-phone' );
$form2 = '';//miracle_get_global_form( 'audit' );
$form3 = miracle_get_global_form( 'send-list' );
include( $theme_dir . 'main/modal.php' );

get_footer();
