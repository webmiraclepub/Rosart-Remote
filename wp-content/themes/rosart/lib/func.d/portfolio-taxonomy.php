<?php

add_action( 'init', 'create_posrtfolio_taxonomies' );

function create_posrtfolio_taxonomies(){

    $labels = array(
		'name' => 'Категория',
		'singular_name' => 'Категория',
		'search_items' =>  'Поиск категории',
		'all_items' => 'Все категории',
		'parent_item' => 'Родительская категория',
		'parent_item_colon' => 'Родительская категория',
		'edit_item' => 'Редактировать категорию',
		'update_item' => 'Обновить категорию',
		'add_new_item' => 'Добавить новую категорию',
		'new_item_name' => 'Новая категория',
		'menu_name' => 'Категория',
	);

	register_taxonomy('design', array('portfolio'), array(
		'hierarchical' => false,
        'public' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true
	));

}
