<?php

///////////////////////////////////////////////
//Отключает сообщения об обновлении плагинов //
///////////////////////////////////////////////

remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
wp_clear_scheduled_hook( 'wp_update_plugins' );
