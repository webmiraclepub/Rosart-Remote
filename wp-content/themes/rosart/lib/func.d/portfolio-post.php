<?php

add_action( 'init', 'cpt_init' );
/**
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function cpt_init() {

	/**
	* --- Проекты разработка
	*/
	$labels = array(
		'name'                 => 'Портфолио',
		'singular_name'        => 'Портфолио',
		'menu_name'            => 'Портфолио',
		'name_admin_bar'       => 'Портфолио',
		'add_new'              => 'Добавить',
		'add_new_item'         => 'Добавить проект в портфолио',
		'new_item'             => 'Новый',
		'edit_item'            => 'Редактировать портфолио',
		'view_item'            => 'Посмотреть',
		'all_items'            => 'Всё портфолио',
		'search_items'         => 'Поиск по портфолио',
		'parent_item_colon'    => 'Родительский проект в портфолио',
		'not_found'            => 'Нет результатов',
		'not_found_in_trash'   => 'Ничего не найдено в корзине',
		'attributes'                    => 'Page Attributes',

		'featured_image' => 'Изображение проекта',
		'set_featured_image' => 'Установить изображение проекта',
		'remove_featured_image' => 'Убрать изображение',
		'use_featured_image' => 'Использовать как изображение проекта',
	);

	$args = array(
		'labels'               => $labels,
		'description'          => '',
		'public'               => true,
		'exclude_from_search'  => false,
		'publicly_queryable'   => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_in_admin_bar'    => true,
		'menu_position'        => 2,
		'menu_icon'            => 'dashicons-admin-page',
		'capability_type'      => 'post',
		'has_archive'          => true,
		'hierarchical'         => false,
		'supports'             => array( 'title', 'author', 'editor', 'thumbnail', 'excerpt', 'revisions' ),
		'rewrite'              => array( 'slug' => 'portfolio' ),
		'taxonomies'        => array( 'develop_cat' )
	);

	register_post_type( 'portfolio', $args );
	add_theme_support( 'post-thumbnails', array( 'post', 'portfolio' ) );

}
