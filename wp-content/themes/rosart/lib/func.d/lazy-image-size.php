<?php

     add_theme_support( 'post-thumbnails' );
    if ( function_exists( 'add_image_size' ) ) {
    	add_image_size( 'lazy', 50, 50, false );
    }
