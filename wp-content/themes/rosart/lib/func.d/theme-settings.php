<?php

///////////////////
// Settings Page //
///////////////////

add_action( 'admin_menu', 'register_my_custom_menu_page' );
function register_my_custom_menu_page(){
	add_menu_page(
		'Настройки организации',
		'Настройки организации',
		'manage_options',
		'miracle-company-settings',
		'my_custom_menu_page',
		'dashicons-id-alt',
		2
	);
}

function my_custom_menu_page(){
	?>
	<h2>Общая информация организации</h2>
	<p>В данном разделе настраиваются общие данные организации, такие как:<p>
	<ul>
		<li><a href="?page=miracle-option-logo">Логотип</a>,</li>
		<li><a href="?page=miracle-option-company-info">Общая информация организации</a>,</li>
		<li><a href="?page=miracle-option-send-app">Блок "Оставить заявку"</a>,</li>
		<li><a href="?page=miracle-option-company-sertificate">Блок "Сертификаты"</a>,</li>
		<li><a href="?page=miracle-option-trust-block">Блок "Нам доверяют"</a>,</li>
		<li><a href="?page=miracle-option-yandex-map">Яндекс карта</a>,</li>
		<li><a href="?page=miracle-option-header">Шапка сайта</a>,</li>
		<li><a href="?page=miracle-option-footer">Футер</a>,</li>
		<li><a href="?page=miracle-option-modal">Модальные окна</a>,</li>
		<li><a href="?page=miracle-option-portfolio">Портфолио</a>,</li>
		<!-- <li><a href="?page=miracle-option-address">Адрес организации</a>,</li>
		<li><a href="?page=miracle-option-social-site">Соц.сети организации</a></li> -->
	</ul>
	<?php
}


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Логотип сайта',
		'menu_title'	=> 'Логотип сайта',
		'menu_slug'  	=> 'miracle-option-logo',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Инф. организации',
		'menu_title'	=> 'Инф. организации',
		'menu_slug'  	=> 'miracle-option-company-info',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Блок "Оставить заявку"',
		'menu_title'	=> 'Блок "Оставить заявку"',
		'menu_slug'  	=> 'miracle-option-send-app',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Блок "Сертификаты"',
		'menu_title'	=> 'Блок "Сертификаты"',
		'menu_slug'  	=> 'miracle-option-company-sertificate',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Блок "Нам доверяют"',
		'menu_title'	=> 'Блок "Нам доверяют"',
		'menu_slug'  	=> 'miracle-option-trust-block',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Яндекс карта',
		'menu_title'	=> 'Яндекс карта',
		'menu_slug'  	=> 'miracle-option-yandex-map',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Шапка сайта',
		'menu_title'	=> 'Шапка сайта',
		'menu_slug'  	=> 'miracle-option-header',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Футер',
		'menu_title'	=> 'Футер',
		'menu_slug'  	=> 'miracle-option-footer',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Модальные окна',
		'menu_title'	=> 'Модальные окна',
		'menu_slug'  	=> 'miracle-option-modal',
		'parent_slug'	=> 'miracle-company-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Портфолио',
		'menu_title'	=> 'Портфолио',
		'menu_slug'  	=> 'miracle-option-portfolio',
		'parent_slug'	=> 'miracle-company-settings',
	));

	// acf_add_options_page(array(
	// 	'page_title' 	=> 'Электронная почта',
	// 	'menu_title'	=> 'Электронная почта',
	// 	'menu_slug'  	=> 'miracle-option-email-address',
	// 	'parent_slug'	=> 'miracle-company-settings',
	// ));
    //
	// acf_add_options_page(array(
	// 	'page_title' 	=> 'График работы',
	// 	'menu_title'	=> 'График работы',
	// 	'menu_slug'  	=> 'miracle-option-work',
	// 	'parent_slug'	=> 'miracle-company-settings',
	// ));
    //
	// acf_add_options_page(array(
	// 	'page_title' 	=> 'Адрес организации',
	// 	'menu_title'	=> 'Адрес организации',
	// 	'menu_slug'  	=> 'miracle-option-address',
	// 	'parent_slug'	=> 'miracle-company-settings',
	// ));
    //
	// acf_add_options_page(array(
	// 	'page_title' 	=> 'Соц.сети организации',
	// 	'menu_title'	=> 'Соц.сети организации',
	// 	'menu_slug'  	=> 'miracle-option-social-site',
	// 	'parent_slug'	=> 'miracle-company-settings',
	// ));
}
