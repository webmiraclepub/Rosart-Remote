<?php

if( ! is_admin() ):
    function miracle_custom_posts_per_page($query){
        if( is_post_type_archive( 'portfolio' ) || is_tax( 'design' ) ):
            $posts_per_page = 6;
            $query->set( 'posts_per_page', $posts_per_page );
        endif;
    }
    add_action('pre_get_posts','miracle_custom_posts_per_page');
endif;
