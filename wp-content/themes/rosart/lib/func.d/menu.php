<?php

add_action('after_setup_theme', 'miracle_register_menu');

function miracle_register_menu(){
    register_nav_menus( array(
        'miracle_menu' => 'Главное меню',
        'miracle_mobile_menu' => 'Мобильное меню',
        'miracle_mobile_footer_menu' => 'Мобильное меню в подвале',
    ) );
}
