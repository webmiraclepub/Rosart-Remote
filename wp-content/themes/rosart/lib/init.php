<?php

add_action('wp_enqueue_scripts', 'miracle_init_css');
function miracle_init_css() {
	// wp_register_style('app', get_template_directory_uri().'/dist/assets/css/app.css', null, null, 'all');
	// wp_enqueue_style('app');
}

add_action('wp_enqueue_scripts', 'miracle_init_js');
function miracle_init_js() {
	wp_deregister_script( 'jquery' );
	if ( is_front_page() ){
		$script_name = 'index-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/index.miracle.js', false, null, true);
	}elseif( is_home() ) {
		$script_name = 'archive-post-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/archivePost.miracle.js', false, null, true);
	}elseif( is_archive() && is_post_type_archive( 'portfolio' ) || is_tax( 'design' ) ){
		$script_name = 'archive-portfolio-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/archivePortfolio.miracle.js', false, null, true);
	}elseif( is_single() && is_singular( 'portfolio' ) ){
		$script_name = 'single-portfolio-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/singlePortfolio.miracle.js', false, null, true);
	}elseif( is_404() ){
		$script_name = 'error-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/errorPage.miracle.js', false, null, true);
	}elseif( is_page_template( 'pagetemplates/services.php' ) ){
		$script_name = 'services-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/services.miracle.js', false, null, true);
	}elseif( is_page_template( 'pagetemplates/contacts.php' ) ){
		$script_name = 'contact-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/contactPage.miracle.js', false, null, true);
	}elseif ( is_page_template( 'pagetemplates/aboutus.php' ) ) {
		$script_name = 'aboutus-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/aboutusPage.miracle.js', false, null, true );
	}elseif ( is_page_template( 'pagetemplates/thanks.php' ) ){
		$script_name = 'thanks-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/thanksPage.miracle.js', false, null, true );
	}elseif( is_single() || is_page() ){
		$script_name = 'single-js';
		wp_register_script( $script_name, get_template_directory_uri().'/dist/single.miracle.js', false, null, true);
	}

	if( isset( $script_name ) && !empty( $script_name ) ){
		wp_enqueue_script( $script_name );
		wp_localize_script( $script_name, 'ajax',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('Rosart')
			)
		);
	}

}

foreach(glob(get_stylesheet_directory() . '/lib/func.d/*.php') as $file) {
	include_once $file;
}

foreach(glob(get_stylesheet_directory() . '/lib/ajax.d/*.php') as $file) {
	include_once $file;
}
