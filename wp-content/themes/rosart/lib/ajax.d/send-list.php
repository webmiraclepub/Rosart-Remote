<?php

add_action('wp_ajax_send-list-form', 'send_list_form_ajax');
add_action('wp_ajax_nopriv_send-list-form', 'send_list_form_ajax');

function send_list_form_ajax() {

	$nonce = $_POST['nonce'];
	if( ! wp_verify_nonce( $nonce, 'Rosart' ) )
		die('Ошибка доступа');

	$to  = get_field( 'miracle-global-modal-3-email', 'option' );
	$subject = get_field( 'miracle-global-modal-3-email-subject', 'option' );
	$message = '
		<html>
		    <head>
		        <title>' . $subject . '</title>
		    </head>
		    <body>'
			. get_field( 'miracle-global-modal-3-email-content', 'option' )
			.'</body>
		</html>';

    	$message = str_replace( '&lt;%name%&gt;', $_POST['name'], $message );
        $message = str_replace( '&lt;%email%&gt;', $_POST['email'], $message );
        $message = str_replace( '&lt;%telephone%&gt;', $_POST['telephone'], $message );
    	$message = str_replace( '&lt;%content%&gt;', $_POST['content'], $message );
        	$message = str_replace( '<%name%>', $_POST['name'], $message );
            $message = str_replace( '<%email%>', $_POST['email'], $message );
            $message = str_replace( '<%site-url%>', $_POST['site_url'], $message );
        	$message = str_replace( '<%content%>', $_POST['content'], $message );

	$headers  = "Content-type: text/html; charset=UTF-8 \r\n";
	$headers .= "From: user <site@" . get_home_url() . ">\r\n";
	$headers .= "Bcc: site@" . get_home_url() . "\r\n";

	mail($to, $subject, $message, $headers);

	echo json_encode( array($to, $subject, $message, $headers) );
	wp_die();
}
