<?php

add_action('wp_ajax_send-phone', 'send_phone_ajax');
add_action('wp_ajax_nopriv_send-phone', 'send_phone_ajax');

function send_phone_ajax() {

	$nonce = $_POST['nonce'];
	if( ! wp_verify_nonce( $nonce, 'Rosart' ) )
		die('Ошибка доступа');

	$to  = get_field( 'miracle-global-modal-1-email', 'option' );
	$subject = get_field( 'miracle-global-modal-1-email-subject', 'option' );
	$message = '
		<html>
		    <head>
		        <title>' . $subject . '</title>
		    </head>
		    <body>'
			. get_field( 'miracle-global-modal-1-email-content', 'option' )
			.'</body>
		</html>';

	$message = str_replace( '&lt;%name%&gt;', $_POST['name'], $message );
	$message = str_replace( '&lt;%telephone%&gt;', $_POST['telephone'], $message );
		$message = str_replace( '<%name%>', $_POST['name'], $message );
		$message = str_replace( '<%telephone%>', $_POST['telephone'], $message );

	$headers  = "Content-type: text/html; charset=UTF-8 \r\n";
	$headers .= "From: user <site@" . get_home_url() . ">\r\n";
	$headers .= "Bcc: site@" . get_home_url() . "\r\n";

	mail($to, $subject, $message, $headers);

	echo mail($to, $subject, $message, $headers);
	wp_die();
}
