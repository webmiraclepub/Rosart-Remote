<?php
    $theme_dir_uri = get_template_directory_uri( );
    $header_style = ( is_page_template( 'pagetemplates/thanks.php' ) ) ? 'style="background-image: url(' . get_template_directory_uri() . '/images/posts/header-bg.jpeg)"' : '';

    $logo_src = get_field( 'miracle-global-logo-image', 'option' )['url'];
    $logo_alt = get_field( 'miracle-global-logo-image', 'option' )['alt'];
    $logo_title = get_field( 'miracle-global-logo-image', 'option' )['title'];
    $after_logo = get_field( 'miracle-global-logo-text', 'option' );
    $address = get_field( 'miracle-global-address', 'option' );
    $shedule = get_field( 'miracle-global-shedule', 'option' );
    $phone = get_field( 'miracle-global-phone-number', 'option' );

    echo get_post_type_archive_link( 'design' );
?>

<div class="header" <?= $header_style ?>>
    <div class="header__top-info">
        <div class="header__top-info-logo logotype">
            <img class="logotype__image" data-src="<?= $logo_src ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>"/>
            <p class="logotype__text"><?= $after_logo ?></p>
        </div>
        <div class="header__top-info-company company-info">
            <i class="company-info__icon miracle-font-adress"></i>
            <p class="company-info__title">Наш адрес</p>
            <p class="company-info__content"><?= $address ?></p>
        </div>
        <div class="header__top-info-company company-info">
            <i class="company-info__icon miracle-font-clock"></i>
            <p class="company-info__title">График работы</p>
            <p class="company-info__content"><?= $shedule ?></p>
        </div>
        <div class="header__top-info-phone firm-contacts">
            <h3 class="firm-contacts__title"><?= $phone ?></h3>
            <a class="firm-contacts__link" href="#" data-modal="miracle-modal-send-phone">Заказать обратный звонок</a>
        </div>
        <button class="header__top-mobile-menu-button" type="button" data-action="mobile-menu">|||</button>
    </div>
    <menu class="header__top-menu top-menu">
        <menuitem  class="top-menu__item">
            <div class="logotype">
                <img class="logotype__image" data-src="<?= $logo_src ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>"/>
            </div>
        </menuitem>
        <button class="header__top-mobile-menu-button" type="button" data-action="mobile-menu">|||</button>
        <?= miracle_get_menuitems( 'miracle_menu' ) ?>
        <menuitem  class="top-menu__item">
            <div class="firm-contacts">
                <h3 class="firm-contacts__title"><?= $phone ?></h3>
            </div>
        </menuitem>
    </menu>

    <menu class="header__mobile-menu mobile-menu">
        <?= miracle_get_menuitems( 'miracle_mobile_menu', false, 'mobile' ) ?>
        <button class="mobile-menu__close-button miracle-font-cross" type="button"></button>
    </menu>
</div>
