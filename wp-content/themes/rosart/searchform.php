<form role="search" method="get" class="menu__search-form" action="<?= home_url( '/' ) ?>">
    <input class="menu__search-input" type="text" placeholder="Search" name="s" value="<?= get_search_query() ?>">
    <button type="button" class="menu__search-icon">&#128269;</button>
</form>
