<div class="archive-body">
    <div class="archive-body__content">
        <div class="archive-body__content-posts">
            <?= $portfolio_posts ?>
        </div>
        <div class="archive-body__pagenation pagenation">
            <a href="#" class="pagenation__item pagenation__prev"><span>Предыдущая</span></a>
            <a href="javascript:void(0)" class="pagenation__item pagenation__page"><span>2</span></a>
            <p class="pagenation__item pagenation__active"><span>3</span></p>
            <a href="javascript:void(0)" class="pagenation__item pagenation__page"><span>6</span></a>
            <p class="pagenation__item pagenation__center"><span>...</span></p>
            <a href="javascript:void(0)" class="pagenation__item pagenation__page"><span>7</span></a>
            <a href="#" class="pagenation__item pagenation__next"><span>Следующая</span></a>
        </div>
    </div>
</div>
