<div class="service-list__service single-body__content">
    <h2>
        <i class="<% icon %>"></i><% title %>
    </h2>
    <div class="service-list__content">
        <div class="service-list__text">
            <% content %>
            <a href="<% link %>" class="miracle-button">Читать подробнее</a>
        </div>
        <img class="alignright" src="<% image-src %>" alt="<% image-alt %>" title="<% image-title %>">
    </div>
</div>
