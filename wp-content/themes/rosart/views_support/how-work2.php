<div class="how-work how-work_hide-mobile" data-src="<?= $theme_dir_uri ?>/src/blocks/how-work/images/background-image2.png">
    <div class="how-work__content">
        <div class="how-work__info">
            <h3 class="miracle-title">Проектирование <br>интерфейсов</h3>
            <p class="miracle-subtitle miracle-subtitle_white">Мы создаем интерактивные прототипы и <br>тщательно согласовываем с вами <br>структуру будущего сайта</p>
            <p class="miracle-content miracle-content_white">Для того, чтобы точно разобраться с контентом сайта и <br>его структурой, мы создаем интерактивные прототипы, <br>которые комфортно можно просматривать в браузере и <br>точно понимать будущий интерфейс</p>
            <button class="miracle-button how-work__button how-work__info_shring" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
    </div>
    <img class="how-work__image" data-src="<?= $theme_dir_uri ?>/src/blocks/how-work/images/image2.png" alt="">
</div>
