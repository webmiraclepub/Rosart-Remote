<div class="contacts-body">
    <div class="contacts-body__content">
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">Наш адрес</h3>
            <p class="contacts-body__info-description">г. Екатеринбург, ул. Советская, 51</p>
        </div>
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">Телефон</h3>
            <p class="contacts-body__info-description">+7 (908) 90-60-681</p>
        </div>
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">E-mail</h3>
            <p class="contacts-body__info-description">info@rosart.pro</p>
        </div>
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">Skype</h3>
            <p class="contacts-body__info-description">Kad6020</p>
        </div>
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">График работы</h3>
            <p class="contacts-body__info-description"><strong>Понедельник-пятница:</strong> 10:00-18:00</p>
            <p class="contacts-body__info-description"><strong>Суббота-воскресенье:</strong> выходные дни</p>
        </div>
        <div class="contacts-body__info">
            <h3 class="contacts-body__info-title">Мы в соц.сетях</h3>
            <div class="contacts-body__social-link">
                <?php
                    if ( function_exists('dynamic_sidebar') ):
                        dynamic_sidebar('social-icon');
                    endif;
                ?>
            </div>
        </div>
    </div>
</div>
