<div class="price-list" data-src="<?= $theme_dir_uri ?>/images/9section.png">
    <div class="price-list__content">
        <h2 class="price-list__block-title miracle-title miracle-title_white">Наши цены</h2>
    </div>
</div>

<template type="price-list-large">
    <div class="price-list__cards price-list-card-content">
        <div class="price-list-card-content__block">
            <i class="price-list-card-content__icon miracle-font-web-development"></i>
            <h3 class="price-list-card-content__title">Разработка<br>сайтов</h3>
            <ul class="price-list-card-content__service-list">
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Создание Landing Page</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Сайты-визитки</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Интернет-магазины</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Корпоративные сайты</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Промо-сайты</li>
            </ul>
            <p class="price-list-card-content__price">от <span class="price-list-card-content__price_color">15 000</span> руб.</p>
            <button class="price-list-card-content__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
        <div class="price-list-card-content__block">
            <i class="price-list-card-content__icon miracle-font-seo2"></i>
            <h3 class="price-list-card-content__title">Продвижение<br>сайтов</h3>
            <ul class="price-list-card-content__service-list">
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>SEO-оптимизация</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Продвижение в социальных сетях</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Таргетированная реклама</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Настройка Яндекс.директ</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Настройка Google AdWords</li>
            </ul>
            <p class="price-list-card-content__price">от <span class="price-list-card-content__price_color">7 000</span> руб.</p>
            <button class="price-list-card-content__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
        <div class="price-list-card-content__block">
            <i class="price-list-card-content__icon miracle-font-design"></i>
            <h3 class="price-list-card-content__title">Дизайн и<br>фирменный стиль</h3>
            <ul class="price-list-card-content__service-list">
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Разработка логотипов</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Создание фирменного стиля</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Наружная реклама</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Визитки, буклеты, флаеры</li>
                <li class="price-list-card-content__service-item"><i class="price-list-card-content__check-icon miracle-font-checkmark"></i>Полиграфическая продукция</li>
            </ul>
            <p class="price-list-card-content__price">от <span class="price-list-card-content__price_color">5 000</span> руб.</p>
            <button class="price-list-card-content__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
    </div>
</template>

<template type="price-list-medium">
    <div class="price-list__cards price-list-tab">
        <div class="price-list-tab__block" data-active="false">
            <div class="price-list-tab__button">
                <i class="price-list-tab__icon miracle-font-web-development"></i>
                <h3 class="price-list-tab__title">Разработка сайтов</h3>
                <i class="price-list-tab__icon-chevron miracle-font-chevron-up"></i>
            </div>
            <div class="price-list-tab__content">
                <div class="price-list-tab__content-left">
                    <ul class="price-list-tab__service-list">
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Создание Landing Page</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Сайты-визитки</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Интернет-магазины</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Корпоративные сайты</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Промо-сайты</li>
                    </ul>
                </div>
                <div class="price-list-tab__content-right">
                    <p class="price-list-tab__price">от <span class="price-list-tab__price_color">15 000</span> руб.</p>
                    <button class="price-list-tab__link-button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
                </div>
            </div>
        </div>
        <div class="price-list-tab__block" data-active="true">
            <div class="price-list-tab__button">
                <i class="price-list-tab__icon miracle-font-seo2"></i>
                <h3 class="price-list-tab__title">Продвижение сайтов</h3>
                <i class="price-list-tab__icon-chevron miracle-font-chevron-up"></i>
            </div>
            <div class="price-list-tab__content">
                <div class="price-list-tab__content-left">
                    <ul class="price-list-tab__service-list">
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>SEO-оптимизация</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Продвижение в социальных сетях</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Таргетированная реклама</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Настройка Яндекс.директ</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Настройка Google AdWords</li>
                    </ul>
                </div>
                <div class="price-list-tab__content-right">
                    <p class="price-list-tab__price">от <span class="price-list-tab__price_color">7 000</span> руб.</p>
                    <button class="price-list-tab__link-button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
                </div>
            </div>
        </div>
        <div class="price-list-tab__block" data-active="false">
            <div class="price-list-tab__button">
                <i class="price-list-tab__icon miracle-font-design"></i>
                <h3 class="price-list-tab__title">Дизайн и фирменный стиль</h3>
                <i class="price-list-tab__icon-chevron miracle-font-chevron-up"></i>
            </div>
            <div class="price-list-tab__content">
                <div class="price-list-tab__content-left">
                    <ul class="price-list-tab__service-list">
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Разработка логотипов</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Создание фирменного стиля</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Наружная реклама</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Визитки, буклеты, флаеры</li>
                        <li class="price-list-tab__service-item"><i class="price-list-tab__check-icon miracle-font-checkmark"></i>Полиграфическая продукция</li>
                    </ul>
                </div>
                <div class="price-list-tab__content-right">
                    <p class="price-list-tab__price">от <span class="price-list-tab__price_color">5 000</span> руб.</p>
                    <button class="price-list-tab__link-button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
                </div>
            </div>
        </div>
    </div>
</template>
