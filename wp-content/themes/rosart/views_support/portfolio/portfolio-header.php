<div style="background-image: url( <?= $bgi_lazy ?> ); background-position: center; background-size: cover; position: relative;">
    <div class="single-header" data-src="<?= $bgi_full ?>">
		<div class="single-header__content">
            <div class="single-header__breadcrumbs breadcrumbs">
                <?= $breadcrumbs ?>
                <p class="breadcrumbs__item breadcrumbs__item_last"><?= $title ?></p>
            </div>
			<p class="single-header__title miracle-title miracle-title_primary"><?= $title ?></p>
			<ul class="single-header__filter archive-filter">
				<li class="archive-filter__filters <?= $active_all ?>" data-term-slug="all">
					<a class="archive-filter__filterlinks" href="<?= $tax_link ?>">Посмотреть все</a>
				</li>
				<?= $filters ?>
			</ul>
		</div>
	</div>
</div>
