<div style="background-image: url( <% image-lazy %> ); background-position: center; background-size: cover; border-radius: 1rem;">
    <div class="post-develop-card" data-src="<% image-full %>">
        <a href="<% link %>">
            <div class="post-develop-card__content">
                <div class="post-develop-card__content-block">
                    <div class="post-develop-card__info">
                        <p class="post-develop-card__info-date"><i class="miracle-font-brif"></i><% date %></p>
                        <p class="post-develop-card__info-time"><i class="miracle-font-clock"></i><% time %></p>
                    </div>
                    <p class="post-develop-card__description"><% title %></p>
                    <button type="button" class="miracle-button">Подробнее</button>
                </div>
            </div>
        </a>
    </div>
</div>
