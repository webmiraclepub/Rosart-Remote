<div class="single-header" data-src="<?= $theme_dir_uri ?>/images/posts/header-bg.jpeg">
	<div class="single-header__content">
		<?php include( $theme_dir . 'breadcrumbs.php' ); ?>
		<p class="single-header__title miracle-title miracle-title_primary">Портфолио</p>
		<ul class="single-header__filter archive-filter">
			<li class="archive-filter__filters archive-filter__filters_active" data-termid="1"><a class="archive-filter__filterlinks" href="#">Посмотреть все</a></li>
			<li class="archive-filter__filters" data-termid="2"><a class="archive-filter__filterlinks" href="#">Создание сайтов</a></li>
			<li class="archive-filter__filters" data-termid="3"><a class="archive-filter__filterlinks" href="#">Дизайн anding page</a></li>
			<li class="archive-filter__filters" data-termid="4"><a class="archive-filter__filterlinks" href="#">Дизайн сайтов-визиток</a></li>
			<li class="archive-filter__filters" data-termid="5"><a class="archive-filter__filterlinks" href="#">Дизайн интернет-магазинов</a></li>
			<li class="archive-filter__filters" data-termid="5"><a class="archive-filter__filterlinks" href="#">Логотипы</a></li>
			<li class="archive-filter__filters" data-termid="6"><a class="archive-filter__filterlinks" href="#">Полиграфия</a></li>
			<li class="archive-filter__filters" data-termid="7"><a class="archive-filter__filterlinks" href="#">Другое</a></li>
		</ul>
	</div>
</div>
