<div class="aboutus__sertificate">
    <div class="miracle-slider__slide">
        <img src="<% image-lazy %>" data-src="<% image-full %>" alt="<% image-alt %>" title="<% image-title %>">
        <i class="miracle-font-zoom miracle-slider__icon-zoom"></i>
    </div>
    <p class="aboutus__sertificate-name"><% title %></p>
    <p class="aboutus__sertificate-description"><% content %></p>
    <% link %>
    <!-- <a class="aboutus__sertificates-url" href="http://ratingruneta.ru/web/yekaterinburg">ratingruneta.ru/web/yekaterinburg</a> -->
</div>
