<div class="aboutus__icon">
    <i class="<% icon %>"></i>
    <p class="aboutus__subheader"><% title %></p>
    <p><% content %></p>
</div>
