<div style="background-image: url( <?= $bgi_lazy ?> ); background-position: center; background-size: cover; position: relative;">
    <div class="single-header" data-src="<?= $bgi_full ?>">
        <div class="single-header__content">
            <div class="single-header__breadcrumbs breadcrumbs">
                <?= $breadcrumbs ?>
                <p class="breadcrumbs__item breadcrumbs__item_last"><?= $title ?></p>
            </div>
            <h1 class="single-header__title miracle-title miracle-title_primary"><?= $title ?></h1>
        </div>
    </div>
</div>

<?php
    if( @get_field( 'single_post_overlay_color' ) && @get_field( 'single_post_overlay_opacity' ) ):
?>
<style>
    .single-header:before{
        background-color: <?= get_field( 'single_post_overlay_color' ) ?><?= dechex( get_field( 'single_post_overlay_opacity' ) ) ?>
    }
</style>
<?php
    endif;
?>
