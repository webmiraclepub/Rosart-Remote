<div class="miracle-modal" data-modal="miracle-modal-send-list" data-src="<% bgi-full %>">
    <div class="miracle-modal__send-form send-form">
        <form>
            <h5 class="send-form__title send-form__title_text-left send-form__title_font-big"><% title %></h5>
            <p class="send-form__subtitle send-form__subtitle_text-left send-form__subtitle_text-gray send-form__subtitle_font-big"><% subtitle %></p>
            <div class="send-form__input-block">
                <label for="f24аt">
                    <p class="send-form__label send-form__label_color-gray">Имя</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f24аt" value="" autocomplete="off" required="required"/>
            </div>
            <div class="send-form__input-block">
                <label for="f24а3t">
                    <p class="send-form__label send-form__label_color-gray">E-mail</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="email"  name="email" id="f24а3t" value="" autocomplete="off" required="required"/>
            </div>
            <div class="send-form__input-block">
                <label for="f112аt">
                    <p class="send-form__label send-form__label_color-gray">Телефон</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="tel" name="telephone" id="f112аt" value="" autocomplete="off" required="required"/>
            </div>
            <div class="send-form__input-block">
                <label for="f112t">
                    <p class="send-form__label send-form__label_color-gray">Текст сообщения</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="text" name="content" id="f112t" value="" autocomplete="off" required="required"/>
            </div>
            <div class="send-form__submit send-form__submit_modal">
                <input type="hidden" name="action" value="send-list-form">
                <button type="submit" class="miracle-button">Отправить</button>
            </div>
        </form>
    </div>
    <button class="miracle-modal__close-button miracle-font-cross" type="button" name="button"></button>
</div>
