<div class="single-body">
    <div class="single-body__content">
        <?php
            wp_reset_postdata();
            the_content();
        ?>
    </div>
</div>
