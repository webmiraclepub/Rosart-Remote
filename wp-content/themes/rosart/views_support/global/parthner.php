<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url( <?= $bgi_lazy ?> ); background-size: cover; background-position: center;">
    <div class="support" data-src="<?= $bgi_full ?>">
        <div class="support__content">
            <h3 class="support__block-title miracle-title"><?= $title ?></h3>
            <div class="support__slider miracle-slider">
                <?= $slides ?>
            </div>
        </div>
    </div>
</div>
