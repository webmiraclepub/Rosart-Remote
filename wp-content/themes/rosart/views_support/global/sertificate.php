<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="sertificate" data-src="<?= $bgi_full ?>">
        <div class="sertificate__content">
            <h3 class="sertificate__block-title miracle-title miracle-title_white"><?= $title ?></h3>
            <div class="sertificate__slider miracle-slider">
                <?= $slides ?>
            </div>
        </div>
    </div>
</div>
