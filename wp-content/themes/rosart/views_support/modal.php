<div class="modal">
    <div class="miracle-modal" data-modal="miracle-modal-send-phone" data-src="<?= $theme_dir_uri ?>/images/modal/2.jpg">
        <div class="miracle-modal__send-form send-form">
            <form class="" action="return false;" method="post">
                <h5 class="send-form__title send-form__title_text-left send-form__title_font-big">Заказать обратный звонок</h5>
                <p class="send-form__subtitle send-form__subtitle_text-left send-form__subtitle_text-gray send-form__subtitle_font-big">Укажите свои контактные данные, и мы свяжемся с вами в ближайшее время</p>
                <div class="send-form__input-block">
                    <label for="f2а">
                        <p class="send-form__label send-form__label_color-gray">Имя</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f2а" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f12а">
                        <p class="send-form__label send-form__label_color-gray">Телефон</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f12а" value="" autocomplete="off"/>
                </div>
                <div class="send-form__submit send-form__submit_modal">
                    <button type="submit" class="miracle-button">Отправить</button>
                </div>
            </form>
        </div>
        <button class="miracle-modal__close-button miracle-font-cross" type="button" name="button"></button>
    </div>
    <div class="miracle-modal" data-modal="miracle-modal-audit" data-src="<?= $theme_dir_uri ?>/images/modal/3.jpg">
        <div class="miracle-modal__send-form send-form">
            <form class="" action="return false;" method="post">
                <h5 class="send-form__title send-form__title_text-left send-form__title_font-big">Получите аудит сайта бесплатно</h5>
                <p class="send-form__subtitle send-form__subtitle_text-left send-form__subtitle_text-gray send-form__subtitle_font-big">Мы выявим сильные и слабые стороны вашего сайта, и покажем над чем стоит работать</p>
                <div class="send-form__input-block">
                    <label for="f24а">
                        <p class="send-form__label send-form__label_color-gray">Имя</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f24а" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f24а3">
                        <p class="send-form__label send-form__label_color-gray">E-mail</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f24а3" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f112а">
                        <p class="send-form__label send-form__label_color-gray">Адрес сайта</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f112а" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f112">
                        <p class="send-form__label send-form__label_color-gray">Текст сообщения</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f112" value="" autocomplete="off"/>
                </div>
                <div class="send-form__submit send-form__submit_modal">
                    <button type="submit" class="miracle-button">Отправить</button>
                </div>
            </form>
        </div>
        <button class="miracle-modal__close-button miracle-font-cross" type="button" name="button"></button>
    </div>
    <div class="miracle-modal" data-modal="miracle-modal-send-list" data-src="<?= $theme_dir_uri ?>/images/modal/1.jpg">
        <div class="miracle-modal__send-form send-form">
            <form class="" action="return false;" method="post">
                <h5 class="send-form__title send-form__title_text-left send-form__title_font-big">Оставьте заявку</h5>
                <p class="send-form__subtitle send-form__subtitle_text-left send-form__subtitle_text-gray send-form__subtitle_font-big">Укажите свои контактные данные, и мы свяжемся с вами в ближайшее время</p>
                <div class="send-form__input-block">
                    <label for="f24аt">
                        <p class="send-form__label send-form__label_color-gray">Имя</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f24аt" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f24а3t">
                        <p class="send-form__label send-form__label_color-gray">E-mail</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f24а3t" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f112аt">
                        <p class="send-form__label send-form__label_color-gray">Адрес сайта</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f112аt" value="" autocomplete="off"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f112t">
                        <p class="send-form__label send-form__label_color-gray">Текст сообщения</p>
                    </label>
                    <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f112t" value="" autocomplete="off"/>
                </div>
                <div class="send-form__submit send-form__submit_modal">
                    <button type="submit" class="miracle-button">Отправить</button>
                </div>
            </form>
        </div>
        <button class="miracle-modal__close-button miracle-font-cross" type="button" name="button"></button>
    </div>
</div>
