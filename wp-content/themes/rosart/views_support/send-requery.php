<div class="send-requery" data-src="<?= $theme_dir_uri ?>/src/blocks/send-requery/images/bg-image.png">
    <div class="send-requery__block">
        <h3 class="send-requery__title miracle-title">Оставьте заявку</h3>
        <h3 class="send-requery__subtitle miracle-title miracle-title_small miracle-title_white">и получите аудит сайта <span class="miracle-title_uppercase miracle-title_blue">Бесплатно</span></h3>
        <p class="send-requery__content miracle-content miracle-content_white">Мы выявим сильные и слабые стороны вашего сайта, и покажем над<br>чем стоит работать, чтобы добиться  результатов в поисковом продвижении</p>
        <button class="send-requery__button miracle-button" type="button" name="button" data-modal="miracle-modal-audit">Оставить заявку</button>
    </div>
</div>
