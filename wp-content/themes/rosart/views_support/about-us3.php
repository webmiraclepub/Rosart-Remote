<div class="about-us about-us_hide-mobile" data-src="<?= $theme_dir_uri ?>/src/blocks/about-us/images/background-image3.png">
	<div class="about-us__container about-us__container_abs-image">
		<img class="about-us__image"  data-src="<?= $theme_dir_uri ?>/src/blocks/about-us/images/nout.png">
		<div class="about-us__info about-us__info_nobotpadding">
			<h2 class="miracle-title">Комплексное <br>продвижение</h2>
			<h4 class="miracle-subtitle">Наши сайты специально <br>оптимизированы под продвижение в <br>поисковых системах</h4>
			<p class="miracle-content">Зачем нужен сайт, если он не дает новых клиентов? Мы <br>можем предложить полный комплекс услуг по <br>продвижению вашего проекта: SEO-оптимизация, SMM, <br>настройку Яндекс.Директ и Google AdWords</p>
			<button class="about-us__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
		</div>
		<div class="about-us__circle-card circle-card">
			<div class="circle-card__block">
				<h5 class="circle-card__block-title"><i class="miracle-font-yandex-google"></i></h5>
				<p class="circle-card__block-content">Яндекс.директ и <br>google adwords</p>
			</div>
			<div class="circle-card__block">
				<h5 class="circle-card__block-title"><i class="miracle-font-smm"></i></h5>
				<p class="circle-card__block-content">Продвижение в <br>социальных сетях</p>
			</div>
			<div class="circle-card__block">
				<h5 class="circle-card__block-title"><i class="miracle-font-seo"></i></h5>
				<p class="circle-card__block-content">Seo-оптимизация</p>
			</div>
		</div>
	</div>
</div>
