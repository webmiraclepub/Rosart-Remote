<div class="about-us about-us_hide-mobile" data-src="<?= $theme_dir_uri ?>/src/blocks/about-us/images/background-image2.png">
    <div class="about-us__container">
        <img class="about-us__image"  data-src="<?= $theme_dir_uri ?>/src/blocks/about-us/images/vizitki.png">
        <div class="about-us__info">
            <h2 class="miracle-title">графический дизайн <br>и фирменный стиль</h2>
            <h4 class="miracle-subtitle">У нас вы можете заказать разработку <br>логотипа, фирменный стиль и дизайн <br>любой печатной продукции</h4>
            <p class="miracle-content">Мы создадим качественный современный логотип, <br>который будет выгодно отличать вас от конкурентов. <br>Также у нас можно заказать дизайн визиток, буклетов, <br>маркетинг-китов, флаеров и пр.</p>
            <button class="about-us__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
    </div>
</div>
