<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="work-step" data-src="<?= $bgi_full ?>">
        <div class="work-step__content">
            <h2 class="work-step__block-title miracle-title"><?= $title ?></h2>
            <div class="work-step__step-card step-card">
                <?= $step_card ?>
            </div>
        </div>
    </div>
</div>
