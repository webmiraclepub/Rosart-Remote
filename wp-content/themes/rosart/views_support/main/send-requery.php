<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="send-requery" data-src="<?= $bgi_full ?>">
        <div class="send-requery__block">
            <h3 class="send-requery__title miracle-title"><?= $title ?></h3>
            <h3 class="send-requery__subtitle miracle-title miracle-title_small miracle-title_white">и получите аудит сайта <span class="miracle-title_uppercase miracle-title_blue">Бесплатно</span></h3>
            <p class="send-requery__content miracle-content miracle-content_white"><?= $content ?></p>
            <button class="send-requery__button miracle-button" type="button" name="button" data-modal="miracle-modal-audit">Оставить заявку</button>
        </div>
    </div>
</div>
