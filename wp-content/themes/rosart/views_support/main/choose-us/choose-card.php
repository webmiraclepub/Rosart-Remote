<div class="choose-card__block">
    <i class="choose-card__icon <% icon %>"></i>
    <h4 class="choose-card__title"><% title %></h4>
    <p class="choose-card__content"><% content %></p>
</div>
