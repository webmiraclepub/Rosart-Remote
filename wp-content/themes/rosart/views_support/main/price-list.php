<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="price-list" data-src="<?= $bgi_full ?>">
        <div class="price-list__content">
            <h2 class="price-list__block-title miracle-title miracle-title_white"><?= $title ?></h2>
        </div>
    </div>
</div>

<template type="price-list-large">
    <div class="price-list__cards price-list-card-content">
        <?= $price_card ?>
    </div>
</template>

<template type="price-list-medium">
    <div class="price-list__cards price-list-tab">
        <?= $price_tab ?>
    </div>
</template>
