<div class="header-slider__slide" style="background-image: url('<% bg-image-lazy %>'); background-size: cover; background-position: center;">
    <div class="header-slider__slide-lazy-bg"  data-src="<% bg-image-full %>">
        <div class="header-slider__content">
            <div class="header-slider__info header-slider__info_show">
                <img class="header-slider__info-logo" src="<% logo-lazy %>" data-src="<% logo-full %>" alt="<% logo-alt %>" title="<% logo-title %>">
                <p class="header-slider__info-title miracle-title miracle-title_white"><% title %></p>
                <p class="header-slider__info-content miracle-content miracle-content_white"><% content %></p>
                <a href="<% link %>" class="header-slider__link miracle-button">Смотреть кейс</a>
            </div>
            <div class="header-slider__bg">
                <img class="header-slider__bg-image" src="<% image-right-lazy %>" data-src="<% image-right-full %>" alt="<% image-right-alt %>" title="<% image-right-title %>">
            </div>
        </div>
    </div>
</div>
