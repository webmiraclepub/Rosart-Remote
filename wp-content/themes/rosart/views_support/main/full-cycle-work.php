<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="how-work" data-src="<?= $bgi_full ?>">
        <div class="how-work__content">
            <div class="how-work__info">
                <h3 class="miracle-title miracle-title_white"><?= $title ?></h3>
                <p class="miracle-subtitle miracle-subtitle_white"><?= $content ?></p>
                <div class="how-work__info-services our-services">
                    <?= $service ?>
                </div>
            </div>
        </div>
        <img class="how-work__image" src="<?= $image_lazy ?>" data-src="<?= $image_full ?>" alt="<?= $image_alt ?>" title="<?= $image_title ?>">
    </div>
</div>
