<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="about-us" data-src="<?= $bgi_full ?>">
        <div class="about-us__container">
            <img class="about-us__image about-us__image_pull-down" src="<?= $image_lazy ?>" data-src="<?= $image_full ?>" alt="<?= $image_alt ?>" title="<?= $image_title ?>">
            <div class="about-us__info">
                <h2 class="miracle-title"><?= $title ?></h2>
                <h4 class="miracle-subtitle"><?= $subtitle ?></h4>
                <p class="miracle-content"><?= $content ?></p>
                <div class="about-us__info-grid info-grid">
                    <?= $info_grid ?>
                </div>
            </div>
        </div>
    </div>
</div>
