<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="how-work how-work_hide-mobile" data-src="<?= $bgi_full ?>">
        <div class="how-work__content">
            <div class="how-work__info">
                <h3 class="miracle-title"><?= $title ?></h3>
                <p class="miracle-subtitle miracle-subtitle_white"><?= $subtitle ?></p>
                <p class="miracle-content miracle-content_white"><?= $content ?></p>
                <button class="miracle-button how-work__button how-work__info_shring" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
            </div>
        </div>
        <img class="how-work__image" src="<?= $image_lazy ?>" data-src="<?= $image_full ?>" alt="<?= $image_alt ?>" title="<?= $image_title ?>">
    </div>
</div>
