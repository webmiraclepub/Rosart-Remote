<div class="our-services__block">
    <h3 class="our-services__title"><% title %></h3>
    <p class="our-services__content"><% content %></p>
</div>
