<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
	<div class="about-us about-us_hide-mobile" data-src="<?= $bgi_full ?>">
		<div class="about-us__container about-us__container_abs-image">
			<img class="about-us__image" src="<?= $image_lazy ?>" data-src="<?= $image_full ?>" alt="<?= $image_alt ?>" title="<?= $image_title ?>">
			<div class="about-us__info about-us__info_nobotpadding">
				<h2 class="miracle-title"><?= $title ?></h2>
				<h4 class="miracle-subtitle"><?= $subtitle ?></h4>
				<p class="miracle-content"><?= $content ?></p>
				<button class="about-us__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
			</div>
			<div class="about-us__circle-card circle-card">
				<?= $circle ?>
			</div>
		</div>
	</div>
</div>
