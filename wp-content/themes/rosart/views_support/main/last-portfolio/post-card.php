<div class="post-develop-card" data-src="<% bgi %>">
    <a href="<% link %>">
        <div class="post-develop-card__content">
            <div class="post-develop-card__content-block">
                <p class="post-develop-card__description"><% title %></p>
                <button type="button" class="miracle-button">Подробнее</button>
            </div>
        </div>
    </a>
</div>
