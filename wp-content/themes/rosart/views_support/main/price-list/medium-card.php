<div class="price-list-tab__block" data-active="<% active %>">
    <div class="price-list-tab__button">
        <i class="price-list-tab__icon <% icon %>"></i>
        <h3 class="price-list-tab__title"><% title %></h3>
        <i class="price-list-tab__icon-chevron miracle-font-chevron-up"></i>
    </div>
    <div class="price-list-tab__content">
        <div class="price-list-tab__content-left">
            <ul class="price-list-tab__service-list">
                <% service-list %>
            </ul>
        </div>
        <div class="price-list-tab__content-right">
            <p class="price-list-tab__price">от <span class="price-list-tab__price_color"><% price %></span> руб.</p>
            <button class="price-list-tab__link-button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
        </div>
    </div>
</div>
