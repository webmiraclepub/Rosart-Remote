<div class="price-list-card-content__block">
    <i class="price-list-card-content__icon <% icon %>"></i>
    <h3 class="price-list-card-content__title"><% title %></h3>
    <ul class="price-list-card-content__service-list">
        <% service-list %>
    </ul>
    <p class="price-list-card-content__price">от <span class="price-list-card-content__price_color"><% price %></span> руб.</p>
    <button class="price-list-card-content__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
</div>
