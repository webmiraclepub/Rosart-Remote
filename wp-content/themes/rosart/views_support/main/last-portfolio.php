<div class="last-portfolio miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" ></div>

<template type="last-portfolio-medium">
    <div class="last-portfolio__content">
        <h3 class="last-portfolio__title miracle-title">Портфолио</h3>
        <p class="last-portfolio__description">Наши последние выполненные проекты "под ключ"</p>

        <div class="last-portfolio__content-posts">
            <?= $main_last_portfolio ?>
        </div>

        <a class="last-portfolio__link miracle-button" href="<?= $portfolio_link ?>">Перейти в портфолио</a>
    </div>
</template>
