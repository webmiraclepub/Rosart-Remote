<div class="circle-card__block">
    <h5 class="circle-card__block-title">
        <i class="<% icon %>"></i>
    </h5>
    <p class="circle-card__block-content"><% content %></p>
</div>
