<div class="info-grid__block">
    <i class="info-grid__block-title <% icon %>" style="<% icon-style %>"></i>
    <p class="info-grid__block-title" style="<% title-style %>"><% title %></p>
    <p class="info-grid__block-content"><% content %></p>
</div>
