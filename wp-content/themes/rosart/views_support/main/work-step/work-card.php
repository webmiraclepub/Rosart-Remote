<div class="step-card__block">
    <div class="step-card__icon" style="background-color: <% color %>">
        <i class="<% icon %>"></i>
    </div>
    <h3 class="step-card__title"><% title %></h3>
    <p class="step-card__content"><% content %></p>
</div>
