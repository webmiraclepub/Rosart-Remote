<div class="header-slider"></div>
<div id="header-slider__info-cards" class="header-slider__info-cards">
    <?= miracle_get_home_header_menu() ?>
</div>

<template type="header-large-slider">
	<div class="header-slider__outer">
		<div class="header-slider__stage">
            <?= miracle_get_header_slider() ?>
		</div>
	</div>
	<div class="header-slider__navs">
		<div class="header-slider__prev"><i class="miracle-font-chevron-left"></i></div>
		<div class="header-slider__next"><i class="miracle-font-chevron-right"></i></div>
	</div>
</template>

<template type="header-medium-slider">
    <div style="background-image: url( <?= $bgi_lazy ?> ); background-size: cover; background-position: center;">
    	<div class="header-medium" data-src="<?= $bgi_full ?>">
    		<img class="header-medium__logo" data-src="<?= $logo_src ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>">
            <h1 class="header-medium__title miracle-title miracle-title_white"><?= $title ?></h1>
    		<p class="header-medium__content"><?= $subtitle ?></p>
    		<button class="header-medium__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
    		<div class="header-medium-slider__info-cards">
                <?= miracle_get_home_header_menu( 'header-medium-slider__info-card' ) ?>
            </div>
    	</div>
    </div>
</template>
