<div class="thanks">
	<i class="miracle-font-checkmark"></i>
	<div>
		<h1>Ваше сообщение успешно отправлено!</h1>
		<p>Мы свяжемся с вами в ближайшее время для уточнения деталей</p>
	</div>
</div>
<script type="text/javascript">
		window.addEventListener( 'load', function(){
			setTimeout( function(){
				location.href = '<?= get_home_url() ?>';
			}, 3500 );
		});
</script>
