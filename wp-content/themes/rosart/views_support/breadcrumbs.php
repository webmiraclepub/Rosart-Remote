<?php
    if( is_singular( 'portfolio' ) ):
?>
<div class="single-header__breadcrumbs single-header__breadcrumbs_portfolio breadcrumbs">
<?php
    else:
?>
<div class="single-header__breadcrumbs breadcrumbs">
<?php
    endif;
?>
    <a href="#" class="breadcrumbs__item">Главная</a>
        <i class="breadcrumbs__chevron">></i>
    <p class="breadcrumbs__item breadcrumbs__item_last">Статьи</p>
</div>
