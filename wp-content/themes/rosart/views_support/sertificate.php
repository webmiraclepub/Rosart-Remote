<div class="sertificate" data-src="<?= $theme_dir_uri ?>/images/12section.png">
    <div class="sertificate__content">
        <h3 class="sertificate__block-title miracle-title miracle-title_white">Дипломы и сертификаты</h3>
        <div class="sertificate__slider miracle-slider">
            <img data-src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" id="1" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/sertificate-2.jpg" id="2" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" id="3" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/sertificate-2.jpg" id="4" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" id="5" alt="">
        </div>
    </div>
</div>


<!-- <div class="miracle-slider">
    <div class="miracle-slider__nav">
        <button class="miracle-slider__dot miracle-slider__dot_active" type="button" data-slide="1"></button>
        <button class="miracle-slider__dot" type="button" data-slide="2"></button>
        <button class="miracle-slider__dot" type="button" data-slide="3"></button>
    </div>
    <div class="miracle-slider__outer">
        <button class="miracle-slider__prev" type="button">-</button>
        <div class="miracle-slider__content">
            <div class="miracle-slider__stage">
                <div class="miracle-slider__slide">
                    <img src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" alt="">
                </div>
                <div class="miracle-slider__slide">
                    <img src="<?= $theme_dir_uri ?>/images/sertificate-2.jpg" alt="">
                </div>
                <div class="miracle-slider__slide">
                    <img src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" alt="">
                </div>
                <div class="miracle-slider__slide">
                    <img src="<?= $theme_dir_uri ?>/images/sertificate-2.jpg" alt="">
                </div>
                <div class="miracle-slider__slide">
                    <img src="<?= $theme_dir_uri ?>/images/sertificate-1.jpg" alt="">
                </div>
            </div>
        </div>
        <button class="miracle-slider__next" type="button">+</button>
    </div>
</div> -->
