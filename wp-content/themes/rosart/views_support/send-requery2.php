<div class="send-requery" data-src="<?= $theme_dir_uri ?>/images/11section.png">
    <div class="send-requery__block">
        <h3 class=" miracle-title">Закажите обратный звонок</h3>
        <p class="miracle-content miracle-content_white">И мы свяжимся с вами в ближайшее время для <br>уточнения деталей</p>
        <button class="send-requery__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Заказать звонок</button>
    </div>
</div>
