<div class="how-work" style="background-image: url(<?= $theme_dir_uri ?>/src/blocks/how-work/images/background-image.min.jpg)" data-src="<?= $theme_dir_uri ?>/src/blocks/how-work/images/background-image.jpg">
    <div class="how-work__content">
        <div class="how-work__info">
            <h3 class="miracle-title miracle-title_white">Полный цикл работ</h3>
            <p class="miracle-subtitle miracle-subtitle_white">Мы предлагаем весь спектр услуг в сфере создания и продвижения сайтов. <br>Перечислим краткий список наших услуг:</p>
            <div class="how-work__info-services our-services">
                <div class="our-services__block">
                    <h3 class="our-services__title">Прототипирование <br>и анализ конкурентов</h3>
                    <p class="our-services__content">Создание интерактиваного прототипа и <br>разработка будущего интерфейса</p>
                </div>
                <div class="our-services__block">
                    <h3 class="our-services__title">Разработка дизайна, логотипа <br>и фирменного стиля</h3>
                    <p class="our-services__content">Разработка дизайна главной страницы и <br>создание макетов внутренних страниц</p>
                </div>
                <div class="our-services__block">
                    <h3 class="our-services__title">Программирование и <br>SEO-оптимизация</h3>
                    <p class="our-services__content">Привязка к системе управления сайтом и <br>продвижение в поисковых системах</p>
                </div>
            </div>
        </div>
    </div>
    <img class="how-work__image" data-src="<?= $theme_dir_uri ?>/src/blocks/how-work/images/image.png" alt="">
</div>
