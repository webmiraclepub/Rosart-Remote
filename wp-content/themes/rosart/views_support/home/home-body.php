<div class="archive-body">
    <div class="archive-body__content">
        <div class="archive-body__content-posts">
            <?= $post_cards ?>
        </div>
        <div class="archive-body__pagenation pagenation">
            <?= $pagenation ?>
        </div>
    </div>
</div>
