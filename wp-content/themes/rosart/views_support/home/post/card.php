<div class="post-card" data-src="<% image-full %>">
    <a href="<% link %>">
        <div class="post-card__content">
            <div class="post-card__info">
                <p class="post-card__info-date"><i class="miracle-font-brif"></i><% date %></p>
                <p class="post-card__info-time"><i class="miracle-font-clock"></i><% time %></p>
            </div>
            <h3 class="post-card__title"><% title %></h3>
            <p class="post-card__description"><% content %></p>
        </div>
    </a>
</div>
