<div class="single-header" data-src="<?= $theme_dir_uri ?>/images/posts/header-bg.jpeg">
    <div class="single-header__content">
        <?php include( $theme_dir . 'breadcrumbs.php' ); ?>
        <h1 class="single-header__title miracle-title miracle-title_primary">Создание сайта для компании по продаже автономных канализаций</h1>
        <p class="single-header__excerpt">Мы привыкли делать свою работу качественно и в четко отведенные сроки. Мы делаем сайты прежде всего для себя, поэтому как никто заинтересованы в уникальности каждого проекта.</p>
        <p class="single-header__link"><a href="#"><i class="miracle-font-earth"></i><span>theseptik.ru</span></a></p>
    </div>
</div>
