<?php
	get_header();

	$theme_dir = get_template_directory( ) . '/views_support/';
	$theme_dir_uri = get_template_directory_uri( );

	include( $theme_dir . 'single-header.php' );
	include( $theme_dir . 'single-body.php' );
	include( $theme_dir . 'home/how-work.php' );
	include( $theme_dir . 'sertificate.php' );
	include( $theme_dir . 'suport.php' );
	include( $theme_dir . 'home/modal.php' );

	get_footer();
?>
