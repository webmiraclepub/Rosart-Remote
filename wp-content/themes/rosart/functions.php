<?php
/*
Obfuscation provided by FOPO - Free Online PHP Obfuscator: http://www.fopo.com.ar/
This code was created on Tuesday, November 14th, 2017 at 9:20 UTC from IP 109.254.254.2
Checksum: bd5640d7d7628f1cf2749d0cff5c49bbb0c50948
*/

include('lib/init.php');


function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

function darken_color($rgb, $darker=2) {

    $hash = (strpos($rgb, '#') !== false) ? '#' : '';
    $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
    if(strlen($rgb) != 6) return $hash.'000000';
    $darker = ($darker > 1) ? $darker : 1;

    list($R16,$G16,$B16) = str_split($rgb,2);

    $R = sprintf("%02X", floor(hexdec($R16)/$darker));
    $G = sprintf("%02X", floor(hexdec($G16)/$darker));
    $B = sprintf("%02X", floor(hexdec($B16)/$darker));

    return $hash.$R.$G.$B;
}

/**
 * Deregister embet js
 */
function my_deregister_scripts(){
 wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

/* --------------------------------------------------------------------------
 * Отключаем Emojii
 * -------------------------------------------------------------------------- */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
add_filter( 'tiny_mce_plugins', 'disable_wp_emojis_in_tinymce' );
function disable_wp_emojis_in_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
/* --------------------------------------------------------------------------- */

/**
 * return elapsed time
 */
function miracle_parse_date( $data_old ){

    $rank = date('U') - $data_old;
    $rank_minute = intval( $rank / 60 );
    $rank_hour = ( $rank_minute > 60 ) ? intval( $rank_minute / 60 ) : 0;
    $rank_day = ( $rank_hour > 24 ) ? intval( $rank_hour / 24 ) : 0;
    $rank_week = ( $rank_day > 7 ) ? intval( $rank_day / 7 ) : 0;
    $rank_mounth = ( $rank_week > 4 ) ? intval( $rank_week / 4 ) : 0;
    $rank_year = ( $rank_mounth > 12 ) ? intval( $rank_mounth / 12 ) : 0;

    if( $rank_year ){
        $year = intval( $rank_mounth / 12 );
        if( $year == 1 ):
            $year = "1 год назад";
        elseif( ( $year%10 == 2 || $year%10 == 3 || $year%10 == 4 ) && $year != 12 && $year != 13 && $year != 14 ):
            $year = $year . " года назад";
        else:
            $year = $year . " лет назад";
        endif;
        $mounth = $rank_mounth % 12;
        $answer = array( $year, $mounth );
    }elseif( $rank_mounth ){
        $mounth = intval( $rank_week / 4 );
        if( $mounth == 1 ):
            $mounth = "1 месяц назад";
        elseif( ( $mounth%10 == 2 || $mounth%10 == 3 || $mounth%10 == 4 ) && $mounth != 12 && $mounth != 13 && $mounth != 14 ):
            $mounth = $mounth . " месяца назад";
        else:
            $mounth = $mounth . " месяцев назад";
        endif;
        $week = $rank_week % 4;
        $answer = array( $mounth, $week );
    }elseif( $rank_week ){
        $week = intval( $rank_day / 7 );
        if( $week == 1 ):
            $week = "1 неделю назад";
        else:
            $week = $week . " недели назад";
        endif;
        $day = $rank_day % 7;
        $answer = array( $week, $day );
    }elseif( $rank_day ){
        $day = intval( $rank_hour / 24 );
        if( $day == 1 ):
            $day = "1 день назад";
        elseif( ( $day%10 == 2 || $day%10 == 3 || $day%10 == 4 ) && $day != 12 && $day != 13 && $day != 14 ):
            $day = $day . " дня назад";
        else:
            $day = $day . " дней назад";
        endif;
        $hour = $rank_hour % 24;
        $answer = array( $day, $hour );
    }elseif( $rank_hour ){
        $hour = intval( $rank_minute / 60 );
        if( $hour == 1 ):
            $hour = "1 час назад";
        elseif( ( $hour%10 == 2 || $hour%10 == 3 || $hour%10 == 4 ) && $hour != 12 && $hour != 13 && $hour != 14 ):
            $hour = $hour . " часа назад";
        else:
            $hour = $hour . " часов назад";
        endif;
        $minute = $rank_minute % 24;
        $answer = array( $hour, $minute );
    }else{
        $minute = intval( $rank / 60 );
        if( $minute == 1 ):
            $minute = "1 минуту назад";
        elseif( ( $minute%10 == 2 || $minute%10 == 3 || $minute%10 == 4 ) && $minute != 12 && $minute != 13 && $minute != 14 ):
            $minute = $minute . " минуты назад";
        else:
            $minute = $minute . " минут назад";
        endif;
        $seconds = $rank % 60;
        $answer = array( $minute, $seconds );
    }

    return $answer;
}
